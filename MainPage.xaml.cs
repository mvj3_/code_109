﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

//http://www.cnblogs.com/cracker/

namespace MessageBoxDemo
{
    /// <summary>
    /// 可用于自身或导航至 Frame 内部的空白页。
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// 在此页将要在 Frame 中显示时进行调用。
        /// </summary>
        /// <param name="e">描述如何访问此页的事件数据。Parameter
        /// 属性通常用于配置页。</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private async void Button_Click_1(object sender, RoutedEventArgs e)
        {
            //MessageBox box = new MessageBox();

            //box.Complete += ((rl) =>
            //{
            //    if (rl == MessageBoxResult.OK)
            //    {
            //        //do work
            //    }

            //});
            //var result = await box.ShowMessage("弹出确定和取消", "提示", MessageBoxButton.OKCancel);

            if (await MessageBox.Show("弹出确定和取消", "提示", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
            {
                MessageBox.Show("点击了确定");
            }

        }

        private async void Button_Click_2(object sender, RoutedEventArgs e)
        {
            //MessageBox box = new MessageBox();

            //box.Complete += ((rl) =>
            //{
            //    if (rl == MessageBoxResult.OK)
            //    {
            //        //do work
            //    }
            //});
            //var result = await box.ShowMessage("只弹出确定");

            MessageBox.Show("只弹出确定");
        }
    }
}
