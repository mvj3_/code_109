众所周知,Windows8已经取消了MessageBox这个类,如果要弹出消息必须用MessageDialog来实现，从Winform到wpf到Silverlight甚至到Wp7一直用了那么多年的MessageBox就这样消失了..取而代之的是更加强大的MessageDialog，虽然MessageDialog很强大..但是用起来并没有MessageBox方便，而绝大多数Windows8 开发者都会拥有WP,Sl,Wpf之类开发经验..在移植方面用起来很不方便,我也有很多不舍....于是今天我来“找回丢失的MessageBox类”。

源码来源：http://code.msdn.microsoft.com/windowsapps/WinRtMessageBox-702fd873/view/SourceCode#content

首先是效果图，然后是几个核心代码，最后是源码